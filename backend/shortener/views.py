from django.core.validators import URLValidator
from django.shortcuts import redirect, render
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from django.views import View
from django.conf import settings
from rest_framework.response import Response

from redis_config.redis import Redis
from .models import ShortModel
from .serializers import ShortsSerializer, ShortPostSerializer


def check_user_session(request):
    if not request.session.session_key:
        request.session.create()
    return request.session.session_key


class ShortenerListAPIView(ListAPIView):
    serializer_class = ShortsSerializer

    def get_queryset(self):
        session_key = check_user_session(self.request)
        return ShortModel.objects.filter(session_key=session_key).order_by('-created')


class ShortenerCreateAPIView(CreateAPIView):
    serializer_class = ShortPostSerializer

    def post(self, request, *args, **kwargs):
        original_url = request.data['original_url']
        short_url = request.data['short_url']
        session_key = check_user_session(request)
        if ShortModel.objects.filter(short_url=settings.HOST_URL + '/' + short_url):
            return Response(
                {'detail': 'Короткое имя не уникально используйте другое'},
                status=status.HTTP_400_BAD_REQUEST)
        if original_url:
            validate = URLValidator()
            try:
                validate(original_url)
            except:
                return Response(
                    {'detail': 'Не корректный URL'},
                    status=status.HTTP_400_BAD_REQUEST)
            data = ShortModel.objects.create(session_key=session_key, original_url=original_url, short_url=short_url)
            short_url = data.short_url
            redis = Redis()
            redis.set(short_url, original_url)
            return Response({'detial': 'Created'}, status=status.HTTP_201_CREATED)
        else:
            return Response(
                {'detail': 'Пустой URL'},
                status=status.HTTP_400_BAD_REQUEST)


class Redirector(View):
    def get(self, request, *args, **kwargs):
        shortener_link = settings.HOST_URL + '/' + self.kwargs['shortener_link']
        redirect_link = ShortModel.objects.filter(short_url=shortener_link).first()
        if redirect_link:
            redirect_link.counter += 1
            redirect_link.save()
            return redirect(redirect_link.original_url)
        return Response({'detial': ''}, 201)
