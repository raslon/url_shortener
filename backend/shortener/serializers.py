from rest_framework.serializers import ModelSerializer
from .models import ShortModel


class ShortPostSerializer(ModelSerializer):
    class Meta:
        model = ShortModel
        fields = ['original_url']


class ShortsSerializer(ModelSerializer):
    class Meta:
        model = ShortModel
        fields = ['short_url', 'original_url', 'counter']
