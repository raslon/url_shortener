import random
import string

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models


class ShortModel(models.Model):
    id = models.AutoField(primary_key=True)
    session_key = models.CharField(max_length=150)
    original_url = models.URLField(blank=False)
    short_url = models.URLField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    counter = models.IntegerField(default=0)

    def __str__(self):
        return self.short_url

    class Meta:
        ordering = ('created',)

    def shortener(self):
        while True:
            random_string = ''.join(random.choices(string.ascii_letters, k=6))
            new_link = settings.HOST_URL + '/' + random_string
            if not ShortModel.objects.filter(short_url=new_link).exists():
                break

        return new_link

    def save(self, *args, **kwargs):
        if not self.short_url:
            new_link = self.shortener()
            self.short_url = new_link
        elif not ShortModel.objects.filter(short_url=self.short_url):
            link = settings.HOST_URL + '/' + self.short_url
            self.short_url = link


        return super().save(*args, **kwargs)
