from django.contrib import admin

from .models import ShortModel


@admin.register(ShortModel)
class ShortModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'short_url', 'original_url', 'created', 'updated', 'counter')
    list_filter = ('created',)