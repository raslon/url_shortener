import logging

from datetime import timedelta
from django.utils import timezone

from backend.celery import app
from backend.settings import URL_TTL

from redis_config.redis import Redis

from shortener.models import ShortModel
logger = logging.getLogger(__name__)

from celery import shared_task


@shared_task
def delete_urls():
    try:
        urls = ShortModel.objects.filter(updated__lte=timezone.now() - timedelta(seconds=URL_TTL))
        redis = Redis()
        for url in urls:
            redis.delete_key(url.short_url)
        urls.delete()
        logger.info(f'delete_old_urls result: {urls}')
    except Exception as ex:
        logger.exception(ex)
