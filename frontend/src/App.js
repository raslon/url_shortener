import React from 'react';
import './App.css';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shortUrlsList: [],
            activeItem: {
                short_url: '',
                original_url: '',
                counter: ''
            },
        }
        this.fetchTasks = this.fetchTasks.bind(this)
        this.originalUrlChange = this.originalUrlChange.bind(this)
        this.shortUrlChange = this.shortUrlChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.getCookie = this.getCookie.bind(this)

    };

    getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    componentWillMount() {
        this.fetchTasks()
    }

    fetchTasks() {
        console.log('Fetching...')
        fetch('http://localhost:8000/api/shorteners/', {credentials: "include"})
            .then(response => response.json())
            .then(data =>
                this.setState({
                    shortUrlsList: data
                })
            )
    }

    originalUrlChange(e) {
        var name = e.target.name
        var value = e.target.value
        console.log('Name:', name)
        console.log('Value:', value)
        this.setState({
            activeItem: {
                ...this.state.activeItem,
                original_url: value
            }
        })
    }

    shortUrlChange(e) {
        var name = e.target.name
        var value = e.target.value
        console.log('Name:', name)
        console.log('Value:', value)
        this.setState({
            activeItem: {
                ...this.state.activeItem,
                short_url: value
            }
        })
    }


    handleSubmit(e) {
        e.preventDefault()
        console.log('ITEM:', this.state.activeItem)

        var csrftoken = this.getCookie('csrftoken')
        var url = 'http://localhost:8000/api/create/'


        fetch(url, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-type': 'application/json',
                'X-CSRFToken': csrftoken
            },
            body: JSON.stringify(this.state.activeItem)
        }).then((response) => {
            this.fetchTasks()
            this.setState({
                activeItem: {
                    original_url: '',
                    short_url: '',
                }
            })
        }).catch(function (error) {
            console.log('ERROR:', error)
        })

    }


    render() {
        var urls = this.state.shortUrlsList
        return (
            <div className="container">

                <div id="task-container">
                    <div id="form-wrapper">
                        <form onSubmit={this.handleSubmit} id="form">
                            <div className="flex-wrapper">
                                <div style={{flex: 6}}>
                                    <input onChange={this.originalUrlChange} className="form-control" id="original_url"
                                           value={this.state.activeItem.original_url} type="text" name="original_url"
                                           placeholder="https://yandex.ru"/>
                                </div>
                                <div style={{flex: 6}}>
                                    <input onChange={this.shortUrlChange} className="form-control" id="short_url"
                                           value={this.state.activeItem.short_url} type="text" name="short_url"
                                           placeholder="Короткая ссылка"/>
                                </div>
                                <div style={{flex: 1}}>
                                    <input id="submit" className="btn btn-warning" type="submit" name="Add"/>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div id="list-wrapper">
                        {urls.map(function (urls, index) {
                            return (
                                <div key={index} className="task-wrapper flex-wrapper">

                                    <div style={{flex: 7}}>

                                        <span>Ссылка</span><br/>
                                        <span>{urls.original_url}</span>

                                    </div>
                                    <div style={{flex: 10}}>
                                        <span>Сокращенная</span><br/>
                                        <span><a href={urls.short_url}>{urls.short_url} </a></span>
                                    </div>

                                    <div style={{flex: 2}}>
                                        <span>Переходы</span><br/>
                                        <span>{urls.counter}</span>
                                    </div>

                                </div>
                            )
                        })}
                    </div>
                </div>

            </div>
        )
    }
}


export default App;